package com.hendisantika.springbootttd.service;

import com.hendisantika.springbootttd.entity.Account;
import com.hendisantika.springbootttd.entity.Client;
import com.hendisantika.springbootttd.exception.InvalidAccountCreationException;
import com.hendisantika.springbootttd.repository.AccountRepository;
import com.hendisantika.springbootttd.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-ttd
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 15/11/18
 * Time: 06.19
 */
@Service
public class AccountService {

    private ClientRepository clientRepository;
    private AccountRepository accountRepository;

    @Autowired
    public AccountService(ClientRepository clientRepository, AccountRepository accountRepository) {
        this.clientRepository = clientRepository;
        this.accountRepository = accountRepository;
    }

    public Account createAccount(String clientNumber) {
        Client client = clientRepository.findByNumber(clientNumber)
                .orElse(clientRepository.save(new Client(clientNumber)));

        if (accountRepository.findByClient(client).isPresent()) {
            throw new InvalidAccountCreationException("Client " + client.getNumber() + " already has an account");
        }

        return accountRepository.save(new Account(client));
    }

}