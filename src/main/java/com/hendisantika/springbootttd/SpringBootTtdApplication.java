package com.hendisantika.springbootttd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootTtdApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootTtdApplication.class, args);
    }
}
