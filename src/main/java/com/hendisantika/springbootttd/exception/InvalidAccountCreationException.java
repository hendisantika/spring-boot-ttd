package com.hendisantika.springbootttd.exception;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-ttd
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 15/11/18
 * Time: 06.12
 */
public class InvalidAccountCreationException extends RuntimeException {

    public InvalidAccountCreationException(String message) {
        super(message);
    }

    public InvalidAccountCreationException(String message, Throwable cause) {
        super(message, cause);
    }
}