package com.hendisantika.springbootttd.exception;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-ttd
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 15/11/18
 * Time: 06.13
 */
public class InvalidClientNameException extends ClientServiceException {

    public InvalidClientNameException() {
        super("An invalid client name was specified");
    }
}