package com.hendisantika.springbootttd.exception;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-ttd
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 15/11/18
 * Time: 06.10
 */
public class ClientNameAlreadyExistsException extends ClientServiceException {

    public ClientNameAlreadyExistsException() {
        super("There is already a client with the specified name");
    }
}