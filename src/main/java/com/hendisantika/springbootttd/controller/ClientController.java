package com.hendisantika.springbootttd.controller;

import com.hendisantika.springbootttd.dto.CreateClientRequest;
import com.hendisantika.springbootttd.entity.Client;
import com.hendisantika.springbootttd.exception.ClientNameAlreadyExistsException;
import com.hendisantika.springbootttd.service.CreateClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-ttd
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 15/11/18
 * Time: 06.18
 */
@RestController
@RequestMapping(value = "/clients")
public class ClientController {

    private CreateClientService createClientService;

    @Autowired
    public ClientController(CreateClientService createClientService) {
        this.createClientService = createClientService;
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Client> createClient(@RequestBody @Valid CreateClientRequest request) {
        Client client = createClientService.createClient(request.getName());
        return ResponseEntity.status(HttpStatus.CREATED).body(client);
    }

    @ExceptionHandler(ClientNameAlreadyExistsException.class)
    ResponseEntity handleClientServiceException(Throwable ex) {
        if (ex instanceof ClientNameAlreadyExistsException) {
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        }

        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

    public void setCreateClientService(CreateClientService createClientService) {
        this.createClientService = createClientService;
    }
}
