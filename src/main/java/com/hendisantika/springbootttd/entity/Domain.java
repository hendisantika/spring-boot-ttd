package com.hendisantika.springbootttd.entity;

import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-ttd
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 15/11/18
 * Time: 06.04
 */
@MappedSuperclass
@Data
public class Domain {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

}