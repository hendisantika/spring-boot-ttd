package com.hendisantika.springbootttd.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import java.util.UUID;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-ttd
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 15/11/18
 * Time: 06.06
 */
@Entity
@Data
@NoArgsConstructor
public class Client extends Domain {

    @Column(nullable = false)
    private String name;

    @Column(nullable = false, unique = true)
    private String number;

    @OneToOne
    private Account account;

    public Client(String name) {
        this.name = name;
        this.number = UUID.randomUUID().toString();
    }
}