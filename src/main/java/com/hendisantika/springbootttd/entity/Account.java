package com.hendisantika.springbootttd.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import java.util.UUID;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-ttd
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 15/11/18
 * Time: 06.07
 */
@Entity
@Data
public class Account extends Domain {

    @Column(nullable = false, unique = true)
    private String code;

    @OneToOne(optional = false)
    private Client client;

    public Account(Client client) {
        this.code = UUID.randomUUID().toString();
        this.client = client;
    }
}