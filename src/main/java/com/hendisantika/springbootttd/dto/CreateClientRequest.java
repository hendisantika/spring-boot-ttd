package com.hendisantika.springbootttd.dto;

import javax.validation.constraints.NotBlank;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-ttd
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 15/11/18
 * Time: 06.17
 */
public class CreateClientRequest {
    @NotBlank
    private String name;

    public CreateClientRequest(String name) {
        this.name = name;
    }

    public CreateClientRequest() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
